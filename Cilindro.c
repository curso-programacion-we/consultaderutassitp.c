#include <stdio.h>

int main() {
    float radio, altura, areatotal;
    const float PI = 3.1416; // Definimos PI
    
    
    printf("Para hallar el área de un cilindro, por favor ingresa la altura: ");
    scanf("%f", &altura);
    
    printf("Para hallar el área de un cilindro, por favor ingresa el radio: ");
    scanf("%f", &radio);
    
    
    areatotal = 2 * PI * radio * (radio + altura); 
    
    
    printf("El area de su cilindro es %.2f\n", areatotal);
    
    return 0;
}

