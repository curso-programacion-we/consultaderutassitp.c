#include <stdio.h>

int main() {

    char* RBuses[] = {"P49", "K905"};
    char* p1[] = {"La campiña", "Toberin"};
    char* p2[] = {"sochalandia", "Calle 72", "Toberin"};
    int RUFest[] = {1, 0};  // si la ruta funciona en feriados=(1) o si no=(0)

    
    printf("consulta de rutas bogota.\n");
    printf("1. ¿Quiere consultar las paradas de la ruta P49\n");
    printf("2. ¿Quiere consultar las paradas de la ruta K905\n");
    printf("3. ¿Quiere consultar las rutas festivas?\n");
    printf("Por favor, elige una opción (1, 2 o 3): ");

    int opcion;
    scanf("%d", &opcion);

    
    if (opcion == 1) {
        printf("\nEstas son las paradas de la ruta P49:\n");
        for (int i = 0; i < sizeof(p1) / sizeof(p1[0]); i++) {
            printf("- %s\n", p1[i]);
        }
    } else if (opcion == 2) {
        printf("\nEstas son las paradas de la ruta K905:\n");
        for (int i = 0; i < sizeof(p2) / sizeof(p2[0]); i++) {
            printf("- %s\n", p2[i]);
        }
    } else if (opcion == 3) {
        printf("\n las rutas que funcionan en días festivos son:\n");
        for (int i = 0; i < sizeof(RBuses) / sizeof(RBuses[0]); i++) {
            if (RUFest[i] == 1) {
                printf("- %s\n", RBuses[i]);
            }
        }
    } else {
        printf("\nPor favor elige una opcion valida: 1, 2 o 3.\n");
    }

    return 0;
}
