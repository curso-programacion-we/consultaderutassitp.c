#include <stdio.h>

int main() {
    float B, b, h, A;
    char opcion = 'S'; //inicializamos la opción para entrar al bucle WHILE

    while(opcion == 'S' || opcion == 's') {
        
        printf("primero digite la longitud de la base mayor en su trapecio a continuacion: ");
        scanf("%f", &B);
        
        printf("digite la longitud de la base menor en su trapecio a continuacion: ");
        scanf("%f", &b);
        
        printf("para finalizar digite la longitud de la altura de su trapecio a continuacion: ");
        scanf("%f", &h);

        
        A = ((B + b) * h) / 2.0;

        
        printf("el area de su trapezoide es %.2f\n", A);

        // el usuario eligira si desea continuar calculando trapezoides o cerrar el programa
        printf("¿Desea calcular otro trapecio? (S/N): ");
        scanf(" %c", &opcion); 
    }

    printf("Hasta Prontoo!\n");

    return 0;
}
